# Caching

Caching is the process of storing the data in a temporary location so that data fetching or retrieval becomes easier. It is done for the data that is frequently used. If the same data is requested again, it becomes a time expensive job to fetch the data multiple times. Caching is usually done for files or information that is stored or present in the system.  

### An example to illustrate the concept
Consider the example of an E-Commerce site. When you visit amazon.com, the first thing a user sees is the home page of amazon. For all the guest users, the same page is displayed. It contains information about various products, notifications about a new product, the current on-going sales, etc. Now when the first user visits the site, all the data is fetched from the system. But what happens when thousands of other users also visit the site. They see the same data. It doesn't make sense to fetch the data again and again from the database or data source. The content that must be displayed, should be stored in an intermediate or cached memory so that data fetching becomes easier.

### How data is stored?
Technically, the cached data is stored in the system's hardware that has faster access like RAM(Random Access Memory). The main purpose of cache memory is faster retrieval of data. It improves the performance of the system.


### Types of Caching :

1. **General Caching**: If an application requires some data to be stored that can be used later, we can store it in a cache. We can then respond faster to the same request next time. We can code our application to cache the response that was generated for the particular request. 
2. **Web Browser Caching**: When a user visits a website, a lot of data is loaded from the site to the user's browser. Now, the browser's cache is used to store most of the content displayed on the webpage. After this, the next time the user visits again, there is no need to load all that data. The browser can easily display the data at a faster rate. When the browser's cache is cleared, it will result in slower loading of the website.
3. **CDN or Content Delivery Network Caching**: A CDN caches content by placing proxy servers closer to the users than the origin servers. When a user living in India, requests Netflix for a particular web series, the servers in India usually sends them the response. This helps Netflix to send the content more quickly. If the user requests for 'Game Of Thrones', a popular web series, the origin server will fetch the content and will store the content in the cache of local proxy servers for future requests.
4. **Database Caching**: A database cache stores frequently accessed read data. It removes unnecessary load from the database. The cache can be in the form of an integrated cache managed within the database engine or in the local cache that stores frequent data within the user's application.
5. **Distributed Caching**: The architecture for distributed caching involves a pool of computers networked together and form a single memory unit. The RAMs of all these computers are pooled together and thus create a distributed system. Data can be stored in these various nodes, and when the request comes, the response is generated from that particular node. The distributed architecture allows incremental expansion and scaling by adding more systems to the system, thus increasing the cache to grow as the data grows in the system.

Wow! Caching seems to solve our performance issues. But should I be concerned while applying caching in my system?

### To Cache or Not to Cache?
Caching requires planning before it is applied in a system. Although it improves the system's performance and increases the user's interaction, it can't be used for huge data. Cache memory is expensive as compared to the slower storage memory. It also takes up extra storage. So we should consider whether the time we are saving is worth the extra space the cached data will take. 

### References Used :
* HTTP Caching.   [https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching]
* What is caching? How it works. [https://aws.amazon.com/caching/]
* Caching - Web Development. [https://youtu.be/n__c7xY1ZcI]

